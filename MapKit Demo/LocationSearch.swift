//
//  LocationFinder.swift
//  MapKit Testing Search Functionality
//

import Foundation
import Combine
import MapKit

class LocationService: NSObject, ObservableObject {
    
    // Different search states
    enum LocationStatus: Equatable {
        case idle
        case noResults
        case isSearching
        case error(String)
        case result
    }
    
    // queryFragment used in view gets updated every time the user types something
    // default status is idle
    // searchResults contains all the results from the queries
    @Published var queryFragment: String = ""
    @Published private(set) var status: LocationStatus = .idle
    @Published private(set) var searchResults: [MKLocalSearchCompletion] = []
    
    private var queryCancellable: AnyCancellable?
    private let searchCompleter: MKLocalSearchCompleter!
    
    // initiate the search completer, set the delegate on self
    init(searchCompleter: MKLocalSearchCompleter = MKLocalSearchCompleter()) {
        self.searchCompleter = searchCompleter
        super.init()
        self.searchCompleter.delegate = self
        
        // receive a stream from the queryFragment in the view
        // debounce (wait) for 500 milliseconds before pushing the event further
        // sink returns the updated string after waiting the specified amount of time
        queryCancellable = $queryFragment
            .receive(on: DispatchQueue.main)
            .debounce(for: .milliseconds(500), scheduler: RunLoop.main, options: nil)
            .sink(receiveValue: { fragment in
                
                // if fragment isn't empty then set the queryFrament to the current updated string
                self.status = .isSearching
                if !fragment.isEmpty {
                    self.searchCompleter.queryFragment = fragment
                } else {
                    self.status = .idle
                    self.searchResults = []
                }
        })
    }
}

// every time the queryFragment gets updated these functions get called
extension LocationService: MKLocalSearchCompleterDelegate {
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        // query the current input, filter out the results that have subtitles
        // results with no subtitles are usually countries and cities
        // remove filter if you want to get points of interest as well
        self.searchResults = completer.results.filter({ $0.subtitle == "" })
        self.status = completer.results.isEmpty ? .noResults : .result
    }
    
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        self.status = .error(error.localizedDescription)
    }
}
