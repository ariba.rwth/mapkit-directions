//
//  MapSearchView.swift
//  MapKit Testing Search Functionality
//

import SwiftUI

class SwiftUIViewHostingController: UIHostingController<ContentView> {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, rootView: ContentView(locationService: LocationService()))
    }
}

struct ContentView: View {
    @ObservedObject var locationService: LocationService
    
    var body: some View {
        VStack {
            Form {
                
                Section(header: Text("Search")) {
                    ZStack(alignment: .trailing) {
                        TextField("Search", text: $locationService.queryFragment)
                        
                        // while user is typing input it sends the current query to the location service
                        // which in turns sets its status to searching; when searching status is set on
                        // searching then a clock symbol will be shown beside the search box
                        if locationService.status == .isSearching {
                            Image(systemName: "clock")
                            .foregroundColor(Color.gray)
                        }
                    }
                }
                
                Section(header: Text("Results")) {
                    List {
                        Group { () -> AnyView in
                            switch locationService.status {
                                case .noResults: return AnyView(Text("No Results"))
                                case .error(let description): return AnyView(Text("Error: \(description)"))
                                default: return AnyView(EmptyView())
                                }
                        }.foregroundColor(Color.gray)
                                       
                        // display the results as a list
                        ForEach(locationService.searchResults, id: \.self) {
                            completionResult in Text(completionResult.title)
                        }
                    }
                }
                
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(locationService: LocationService())
    }
}
