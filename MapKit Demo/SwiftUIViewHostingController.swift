//
//  SwiftUIViewHostingController.swift
//  Location Tutorial
//
//  Created by Brian Porumb on 28.11.21.
//

import Foundation



struct ContentView: View {
    @ObservedObject var locationService: LocationService
    
    var body: some View {
        VStack {
            Form {
                
                Section(header: Text("Search")) {
                    ZStack(alignment: .trailing) {
                        TextField("Search", text: $locationService.queryFragment)
                        if locationService.status == .isSearching {
                            Image(systemName: "clock")
                            .foregroundColor(Color.gray)
                        }
                    }
                }
                
                Section(header: Text("Results")) {
                    List {
                        Group { () -> AnyView in
                            switch locationService.status {
                                case .noResults: return AnyView(Text("No Results"))
                                case .error(let description): return AnyView(Text("Error: \(description)"))
                                default: return AnyView(EmptyView())
                                }
                        }.foregroundColor(Color.gray)
                                                    
                        ForEach(locationService.searchResults, id: \.self) {
                            completionResult in Text(completionResult.title)
                        }
                    }
                }
                
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(locationService: LocationService())
    }
}
