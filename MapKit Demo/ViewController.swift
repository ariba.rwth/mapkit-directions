//
//  ViewController.swift
//  MapKit Routing, Annotations and Location
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    var regionInMeters: Double = 10000 //scale of the map
    var directionsArray: [MKDirections] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkLocationServices()
        setupMapView()
    }
    
    func setupMapView(){
        mapView.delegate=self
    }
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    // helper function to display alerts to user
    func displayAlert(alertTitle: String, alertMessage: String) {
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
        
    func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        }
    }
    
    // check if user's location services are enabled
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
        } else {
            print("Location Services are not enabled")
            displayAlert(alertTitle: "Location Services are not turned on", alertMessage: "You must enable your Location Services to perform this action")
        }
    }
    
    // checking authorization status
    func checkLocationAuthorization() {
        let manager = CLLocationManager()
        switch manager.authorizationStatus {
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
            centerViewOnUserLocation()
            locationManager.startUpdatingLocation()
            break
        case .denied:
            displayAlert(alertTitle: "Unable to access location", alertMessage: "The app does not have access to location because the user denied")
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            displayAlert(alertTitle: "Unable to access location", alertMessage: "The app does not have access to location because of Device Settings(like Parental Controls)")
            break
        case .authorizedAlways:
            break
        @unknown default:
            break
        }
    }
    
    // action triggered on the "GO" button tap
    @IBAction func goButtonTapped(_ sender: Any) {
        print("GO button tapped")
        getDirections()
    }
    
    //to prevent throttling and multiple renders
    func resetMapView(withNew directions: MKDirections) {
        mapView.removeOverlays(mapView.overlays)
        directionsArray.append(directions)
        let _ = directionsArray.map { $0.cancel() }
    }
    
    // main function for routing
    func getDirections() {
        guard let location = locationManager.location?.coordinate else {
            print("Location not found")
            displayAlert(alertTitle: "Location not found", alertMessage: "App cannot access your location")
            return
        }
        
        let request = createDirectionsRequest(from: location)
        let directions = MKDirections(request: request)
        resetMapView(withNew: directions)
        
        directions.calculate { [unowned self] (response, error) in
            if error != nil {
                print("Directions couldn't be computed")
                displayAlert(alertTitle: "Directions couldn't be computed", alertMessage: "App cannot find directions to the destination")
                return
            }
            guard let response = response else {
                print("Response not found")
                displayAlert(alertTitle: "Directions couldn't be computed", alertMessage: "App cannot find directions to the destination")
                return
            }
            print("total routes found: \(response.routes.count)")
            for route in response.routes {
                self.mapView.addOverlay(route.polyline, level: .aboveRoads) //add an overlay for each route
                                                                            //polyline: connected line segments that don't form a loop
                var directionsRegion: MKCoordinateRegion = MKCoordinateRegion.init(route.polyline.boundingMapRect)  // recenter
                //to increase the span of the region in order to add some margin
                directionsRegion.span.latitudeDelta *= 1.2
                directionsRegion.span.longitudeDelta *= 1.2
                self.mapView.setRegion(directionsRegion, animated: true)
            }
        }
    }
    
    // creates a new request by configuring different properties
    func createDirectionsRequest(from coordinate: CLLocationCoordinate2D) -> MKDirections.Request {
        let destinationCoordinate       = getLocationCoordinates()
        let startingLocation            = MKPlacemark(coordinate: coordinate) //user's location
        let destination                 = MKPlacemark(coordinate: destinationCoordinate)
        let request                     = MKDirections.Request()
        request.source                  = MKMapItem(placemark: startingLocation)
        request.destination             = MKMapItem(placemark: destination)
        request.transportType           = .automobile
        request.requestsAlternateRoutes = true
        return request
    }
    
    // hard-coded to return coordinates of Southampton
    func getLocationCoordinates() -> CLLocationCoordinate2D {
        let southampton = MKPointAnnotation()
        southampton.title = "Southampton"
        southampton.coordinate = CLLocationCoordinate2D(latitude: 50.9097, longitude: -1.4044)
        mapView.addAnnotation(southampton) // add annotation(a pin) for Southampton
        return southampton.coordinate
    }
    
}

extension ViewController: CLLocationManagerDelegate, MKMapViewDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let region = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
        mapView.setRegion(region, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
    
    // called each time the bounding map rectangle of an overlay intersects the visible region of a map
    // defines 'how' the overlay should look like -> renderer properties are specified here
    func mapView(_ mapView: MKMapView,
                 rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red.withAlphaComponent(0.5)
        renderer.lineWidth = 7
        print("renderer for overlay")
        return renderer
    }
    
    // Delegate method for annotations
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        let identifier = "Annotation"
        // dequeue an existing annotation view before creating a new one as new annotations move on-screen
        // (performance enhancement)
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        return annotationView
    }
    
}


