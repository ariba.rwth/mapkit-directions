# MapKit Directions 

This project (a part of iOS App Dev W21 coursework) displays routes in MapKit from a user's location to a hard-coded location of "Cologne".

Result - 
![img](https://git.rwth-aachen.de/ariba.rwth/mapkit-directions/-/blob/main/Simulator_Screen_Shot_-_iPhone_11_Pro_Max_-_2021-11-27_at_02.29.26.png)

